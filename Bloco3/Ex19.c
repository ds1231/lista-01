/*
Escreva um algoritmo que leia um numero inteiro entre 100 e 999 e imprima na saida cada um dos 
algarismos que compoem o numero
*/

#include <stdio.h>

int num, aux, dezena;

void main () {

    while (num < 100 || num > 999) {
        printf ("Digite um numero entre 100 e 999: ");
        scanf ("%d", &num);
    }


    aux = num / 100;
    printf ("Centena: %d\n", aux);


    aux = aux * 100;
    aux = num - aux;

    while (aux > 10) {
        aux = aux - 10;
        dezena ++;
    }

    printf ("Dezena: %d\n", dezena);

//Unidade
aux = num / 100 * 100;

/*
printf ("aux = num / 100 * 100;\n");
printf ("aux vale: %d\n", aux);
printf ("num vale: %d\n", num);
*/

dezena = dezena * 10;

/*
printf ("\ndezena = dezena * 10\n");
printf ("dezena vale: %d\n", dezena);
*/

aux = aux + dezena;

/*
printf ("\naux = aux + dezena;\n");
printf ("aux vale: %d\n", aux);
printf ("num vale: %d\n", num);
*/

if (num > aux) {
aux = num - aux;
} else {
aux = aux - num;
}

printf ("Unidade: %d\n", aux);
}