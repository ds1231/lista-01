/*
Faça um programa que leia um número inteiro positivo N e imprima todos os números naturais de 0 até N em ordem decrescente
*/

#include <stdio.h>

int num;

void main () {

printf ("Digite um numero: ");
scanf ("%d", &num);

while (num != 0) {
    printf ("%d\n", num);
    num--;
}
}