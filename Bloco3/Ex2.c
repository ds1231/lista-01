/*
Escreva um programa que escreva na tela, de 1 ate 100, de 1 em 1, 3 vezes. A primeira vez deve usar a estrutura de repeticao
for, a segunda while, e a terceira do while
*/
#include <stdio.h>

int i;

void main () {

printf ("For\n");
    for (i=1; i<=100; i++) {
        printf ("%d ", i);
    }

printf ("\nWhile\n");
i = 0;
    while (i != 100) {
        i++;
        printf ("%d ", i);
    
    }

printf ("\nDo while\n");
i = 0;
do {
    i++;
    printf ("%d ", i);
} while (i != 100);

}