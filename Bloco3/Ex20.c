/*
Ler uma sequência de números inteiros e determinar se eles são pares ou não. Deverá ser informado o número de dados lidos e
número de valores pares. O processo termina quando for digitado o número 1000.
*/

#include <stdio.h>

int quant, num, pares;
int main () {
printf ("Digite a quantidade de numeros para ser impressos: ");
scanf ("%d", &quant);

    while (quant != 0) {
    printf ("Digite um número: ");
    scanf ("%d", &num);

        if (num == 1000) {
            return -1;
        } else {
            if (num % 2 == 0) {
                pares++;
                printf ("Par. numeros pares: %d\n", pares);
            } else {
                printf ("Impar. numeros pares: %d\n", pares);
            }
        }
    quant--;
    }
    return 0;
}