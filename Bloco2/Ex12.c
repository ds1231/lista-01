/*
Escreva um programa que leia um numero inteiro maior do que zero e devolva, na tela, a soma de todos os seus algarismos.
Por exemplo, ao numero 251 correspondera o valor 8 (2 + 5 + 1). Se o numero lido nao for maior do que zero, o programa
terminara com a mensagem "Numero invalido"
*/

#include <stdio.h>

int num, soma;

int main () {

printf ("Digite um numero: ");
scanf ("%d", &num);

if (num <= 0) {
    printf ("Numero invalido");
    return -1;
}
while (num  > 0) {
    soma += num % 10;
    num /= 10;
}

printf ("A soma dos algarismos é %d\n", soma);

return 0;
}