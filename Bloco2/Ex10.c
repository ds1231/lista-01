/*
Faca um programa que receba a altura e o sexo de uma pessoa de uma pessoa e calcule e mostre seu peso ideal,
utilizando as seguintes formulas (onde h corresponde à altura):

Homens: (72.7 * h) - 58
Mulheres (62.1 * h - 44.7)
*/

#include <stdio.h>

char sexo;
float h, peso;

void main () {

printf ("Digite sua altura: ");
scanf ("%f", &h);

printf ("Digite o sexo:\nM = Masculino\nF - Feminino\n");
while (getchar () != '\n');
scanf ("%c", &sexo);

if (sexo == 'm' || sexo == 'M') {
peso = 72.7 * h;
peso = peso - 58;
} else {
peso = 62.1 * h;
peso = peso - 44.7;
}

printf ("Peso recomendado: %.2f KG", peso);

}