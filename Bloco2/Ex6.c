//Escreva um programa que, dados dois numeros inteiros, mostre na tela o maior deles, assim como a diferença entre ambos.
#include <stdio.h>

int num1, num2, dif;

void main () {
    
printf ("Digite um numero: ");
scanf ("%d", &num1);

printf ("Digite outro numero: ");
scanf ("%d", &num2);

    if (num1 > num2) {
        dif = num1 - num2;
        printf ("%d é o maior. Diferença: %d", num1, dif);
    } else {
        dif = num2 - num1;
        printf ("%d é o maior. Diferença: %d", num2, dif);  

    }

}