/*
Faça um programa que receba dois numeros e mostre o maior. Se por acaso, os dois numeros forem iguais, imprima a mensagem
Numeros iguais.
*/
#include <stdio.h>

int num1, num2;

void main () {
printf ("Digite um numero: ");
scanf ("%d", &num1);
printf ("Digite outro numero: ");
scanf ("%d", &num2);

    if (num1 > num2) {
        printf ("%d é maior", num1);
    } else if (num2 > num1) {
        printf ("%d é maior", num2);
    } else {
        printf ("Numeros iguais");
    }

}