#include <stdio.h>

float kelvin, celsius;

void main () {
    printf ("Digite a temperatura em Kelvin: ");
    scanf ("%f", &kelvin);
    celsius = kelvin - 273.15;
    printf ("Temperatura em Celsius: %.2f ºC", celsius);
}